#!/usr/bin/env python3
import os
from optparse import OptionParser

import pywikibot
import requests


def download(pagename, filename):
    url = "https://www.wikidata.org/w/index.php?title=%s&action=raw" % pagename
    r = requests.get(url)
    assert(r.status_code == 200)
    with open(filename, 'w') as f:
        f.write(r.text)


def upload(pagename, filename, comment="New version"):
    with open(filename) as f:
        site = pywikibot.Site()
        page = pywikibot.Page(site, pagename)
        page.text = f.read()
        page.save(comment)


parser = OptionParser()
parser.add_option("-c", "--comment", dest="comment",
                  help="edit summary", metavar="TEXT")
parser.add_option("-p", "--page", dest="pagename",
                  help="mediawiki pagename", metavar="PAGENAME")
parser.add_option("-u", "--username", dest="username",
                  default="MichaelSchoenitzer",
                  help="Wikimedia username", metavar="USER")
parser.add_option("--prefix", dest="prefix", default="",
                  help="pagename-prefix", metavar="PREFIX")
parser.usage += " action filename"

(options, args) = parser.parse_args()

if len(args) != 2:
    parser.print_help()
    exit(-1)

options.filename = args[1]
if options.filename[0:4] == "src/":
    options.fullfilename = options.filename
    options.filename = options.filename[4:]
else:
    options.fullfilename = "src/" + options.filename

if options.pagename is None:
    options.pagename = "User:{}{}/{}".format(options.username,
                                             options.prefix,
                                             options.filename)
abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)


if args[0] == "download":
    download(options.pagename, options.fullfilename)
elif args[0] == "upload":
    upload(options.pagename, options.fullfilename, options.comment)
else:
    print("invalid command")
