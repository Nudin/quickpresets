quick_props = [
	
	{
		qid: 5,
		name: "Human",
		defaults: [
			{
				name: "Gender",
				pid: 21,
				values: [
					{name: "male", qid: 6581097},
		            {name: "female", qid: 6581072},
		            {name: "Intersexual", qid: 1097630},
		            {name: "Transfemale", qid: 1052281},
		            {name: "Transmale", qid: 2449503},
		            {name: "Genderqueer", qid: 48270},
		           ]
			},
			{
				name: "Occupation",
				pid: 106,
				values: [
		            {name: "Politican", qid: 82955},
		            {name: "soccer player", qid: 937857},
		            {name: "actor", qid: 33999},
		            {name: "writer", qid: 36180},
		            {name: "painter", qid: 1028181},
					{name: "physicist", qid: 169470},
		           ]
			},
			{
				name: "Citizenship",
				pid: 27,
				values: [
					{name: "USA", qid: 30},
		            {name: "Germany", qid: 183},
		            {name: "France", qid: 142},
		            {name: "UK", qid: 145},
		            {name: "Japan", qid: 17},
		            {name: "Spain", qid: 29},
		           ]
			},
			{
				name: "Language",
				pid: 1412,
				values: [
					{name: "English", qid: 1860},
		            {name: "German", qid: 188},
		            {name: "French", qid: 150},
		            {name: "Spanish", qid: 1321},
		            {name: "Russian", qid: 7737},
		           ]
			},
		]
	},
	{
		qid: 7397,
		name: "Software",
		defaults: [
			{
				name: "License",
				pid: 275,
				values: [
					{name: "GPL", qid: 7603},
		            {name: "GPLv2+", qid: 27016752},
		            {name: "GPLv3", qid: 10513445},
		            {name: "MIT", qid: 334661},
		            {name: "LGPL", qid: 192897},
		            {name: "BSD", qid: 191307},
		            {name: "Apache", qid: 616526},
		            {name: "CC-BY-SA 3.0", qid: 14946043},
		            {name: "Mozilla", qid: 308915},
		           ]
			},
			{
				name: "Os",
				pid: 306,
				values: [
					{name: "Linux", qid: 388},
					{name: "Windows", qid: 1406},
					{name: "macOS", qid: 14116},
					{name: "Plattformunabhäng", qid: 174666},
					{name: "unixoid", qid: 14656},
		    		]
			},
			{
				name: "Language",
				pid: 277,
				values: [
					{name: "c", qid: 15777},
					{name: "c++", qid: 2407},
					{name: "Python", qid: 28865},
					{name: "java", qid: 251},
					{name: "PHP", qid: 59},
					{name: "javascript", qid: 2005},
					{name: "Perl", qid: 42478},
					{name: "Assembler", qid: 165436},
					{name: "C#", qid: 2370},
					{name: "Ruby", qid: 161053},
		    		]
			},
			{
				name: "Gui",
				pid: 1414,
				values: [
					{name: "Qt", qid: 201904},
					{name: "Gtk+", qid: 189464},
					{name: "wxWidgets", qid: 284982},
					{name: "ncurses", qid: 310974},
		    		]
			},
		]
	}
];