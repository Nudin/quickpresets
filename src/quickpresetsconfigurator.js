$(function() {

function basetype(type) {
	if(type === "wikibase-item")
		return "item";
	else if(type === "string" || type === "external-id" || type === "url")
		return "string";
	else
		return type;
}

function addSetting() {
	var idx = settings.length;
	settings[idx] = { qid: null, name: "", defaults: [] };
	var setting = settings[idx];
	
	var div = $("<div></div>");
	div.appendTo('#configuratorsettings')
		.addClass('qpcborder');
	var p = $("<p>For wich types of itmes should this preset group be shown (incl. subtypes of the selected)</p>").appendTo(div);
	var option1 = new OO.ui.RadioOptionWidget( {
	  data: 'type',
	  label: 'An item with some P31'
	} );
	var option2 = new OO.ui.RadioOptionWidget( {
	  data: 'any',
	  label: 'An item with any P31'
	} );
	var option3 = new OO.ui.RadioOptionWidget( {
	  data: 'none',
	  label: 'An item without any P31'
	} );
	var radioSelect = new OO.ui.RadioSelectWidget( { 
	  items: [ option1, option2, option3 ] 
	} );
	radioSelect.selectItem( option1 );
	div.append( radioSelect.$element );
	
	var update = function(input) {
			if(radioSelect.findSelectedItem().data === "type") {
				setting.name = $(input.target).data('entityselector').selectedEntity().label;
				setting.qid = $(input.target).data('entityselector').selectedEntity().id;
			}
			else if(radioSelect.findSelectedItem().data === "any") {
				setting.name = "Any";
				setting.qid = "0";
			}
			else if(radioSelect.findSelectedItem().data === "none") {
				setting.name = "No P31";
				setting.qid = null;
			}
			updateconf();
		};
	
	var sdiv = $("<div class='defaults'></div>");
	var entitysel = $("<input>")
		.entityselector( {
			    url: 'https://www.wikidata.org/w/api.php',
			    language: mw.config.get('wgUserLanguage')
			} )
		.appendTo(div)
		.on('entityselectorselected', update);
		
	radioSelect.on('select', function(event) {
		if(event.data === 'type')	{
			entitysel.show();
		}
		else {
			entitysel.hide();
		}
		update(event);
	});
	var btn = new OO.ui.ButtonWidget( {
		label: 'Add Property'
		} );
	$(btn.$element)
		.appendTo(div)
		.on('click', function(event) {
			var idx = setting.defaults.length;
			setting.defaults[idx] = {name: "", pid: null};
			var parent = btn.$element.parent().find('.defaults');
			addDefault(parent, setting.defaults, idx);
			updateconf();
			});
	var delico = new OO.ui.IconWidget( {
		icon: 'trash',
		flags: 'destructive',
		title: 'Remove Preset-Group'
	} );
	$(delico.$element)
		.appendTo(p)
		.on('click', function(input) { 
			removeSetting(delico.$element.parent().parent()[0], settings);
		} );
	sdiv.appendTo(div);
}

function removeSetting(elem, defaults) {
	var nodes = Array.prototype.slice.call(elem.parentElement.children);
	var idx = nodes.indexOf(elem);
	defaults.splice(idx, 1);
	elem.remove();
	updateconf();
}

function addDefault(parent, defaults, idx) {
	var def = defaults[idx];
	def.values = [];
	var div = $("<div>Property:</div>")
		.addClass('qpcborder')
		.appendTo(parent);
	var pdiv = $("<div class='presets'></div>");
	var input = $("<input>")
		.entityselector( {
			    url: 'https://www.wikidata.org/w/api.php',
			    language: mw.config.get('wgUserLanguage')
			} )
		.appendTo(div)
		.on('entityselectorselected', function(input) {
			def.name = $(input.target).data('entityselector').selectedEntity().label;
			def.pid = $(input.target).data('entityselector').selectedEntity().id;
			updateconf();
		} );
	input.data('entityselector').options.type = "property";
	var btn = new OO.ui.ButtonWidget( {
		label: 'Add Default-Value'
		} );
	$(btn.$element)
		.appendTo(div)
		.on('click', function(event) {
			if (input.data('entityselector').selectedEntity() === null)
				return;
			var type = basetype(input.data('entityselector').selectedEntity().datatype);
			def.type = type;
			var idx = def.values.length;
			def.values[idx] = {};
			parent = btn.$element.parent().find('.presets');
			addPreference(parent, def.values, idx, type);
			updateconf();
			});
	var delico = new OO.ui.IconWidget( {
		icon: 'trash',
		flags: 'destructive',
		title: 'Remove Preset'
	} );
	$(delico.$element)
		.appendTo(div)
		.on('click', function(input) { 
			removeDefault(delico.$element.parent()[0], defaults);
		} );
	pdiv.appendTo(div);
}

function removeDefault(elem, defaults) {
	var nodes = Array.prototype.slice.call(elem.parentElement.children);
	var idx = nodes.indexOf(elem);
	defaults.splice(idx, 1);
	elem.remove();
	updateconf();
}

function addPreference(parent, prefs, idx, type) {
	var pref = prefs[idx];
	var div = $("<div>Value:</div>")
		.appendTo(parent);
	if(type === "item") {
		$("<input>")
			.entityselector( {
				    url: 'https://www.wikidata.org/w/api.php',
				    language: mw.config.get('wgUserLanguage')
				} )
			.appendTo(div)
			.on('entityselectorselected', function(input) {
				pref.name = $(input.target).data('entityselector').selectedEntity().label;
				pref.qid = $(input.target).data('entityselector').selectedEntity().id;
				updateconf();
			} );
	}
	else if(type === "string") {
		$("<input>")
			.appendTo(div)
			.on('change', function(input) {
				pref.text = input.target.value;
				updateconf();
			} );
	}
	else {
		$("<p></p>")
			.appendTo(div)
			.text("Property-typ not supported.");
	}
	var delico = new OO.ui.IconWidget( {
		icon: 'trash',
		flags: 'destructive',
		title: 'Remove Preset-value'
	} );
	$(delico.$element)
		.appendTo(div)
		.on('click', function(input) { 
			removePreference(delico.$element.parent()[0], prefs);
		} );
}

function removePreference(elem, prefs) {
	var nodes = Array.prototype.slice.call(elem.parentElement.children);
	var idx = nodes.indexOf(elem);
	prefs.splice(idx, 1);
	elem.remove();
	updateconf();
}

function updateconf() {
	$('#qpcconfig')[0].value=JSON.stringify(settings, null, 2);
}

if (document.baseURI !== "https://www.wikidata.org/wiki/Special:ConfigQuickpresets") {
	return;
}
document.getElementById('firstHeading').innerHTML="Config Quickpresets";
document.title = "Config Quickpresets – Wikidata";
document.getElementById('mw-content-text').innerHTML="";

	
settings = [];

$("<p>This page allows you to create a config for <a href='https://www.wikidata.org/wiki/User:MichaelSchoenitzer/quickpresets'>Quickpresets</a></p>")
	.appendTo('#mw-content-text');
	
$("<div id='configurator'></div>")
	.appendTo('#mw-content-text');
	
	
var btn = new OO.ui.ButtonWidget( {
	label: 'Add Preset-Group'
	} );
$(btn.$element)
	.appendTo('#configurator')
	.on('click', addSetting );
$('#configurator').append("<div id='configuratorsettings'></div>");

$("<textarea></textarea>")
	.attr("id","qpcconfig")
	.appendTo('#mw-content-text');
	
var configPage = "User:"+mw.config.get("wgUserName")+"/quickpresets settings.json";
var configPageLink = "https://www.wikidata.org/wiki/" + configPage;
$("<p>Save this configuration as <a href=''></a></p>")
	.appendTo('#mw-content-text')
	.find("a").attr('href', configPageLink)
	.text(configPage);

});